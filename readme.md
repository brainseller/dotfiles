# .dotfiles

to setup a bash to my need

## usage

If you don't know how to use them, you probably shouldn't.
But you can learn about them on the following sites:

http://www.oliverelliott.org/article/computing/tut_unix/

http://code.tutsplus.com/tutorials/setting-up-a-mac-dev-machine-from-zero-to-hero-with-dotfiles--net-35449

I also wrote a bit on it http://frankfuchs.net/articles/12/dotfiles-hooray
